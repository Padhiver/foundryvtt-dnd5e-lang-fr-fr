# FoundryVTT dnd5e lang fr-FR

## Ce module traduit les clés (termes) exposés par le créateur pour ce system de jeu DND5E. 
		
## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)

## Installation : 
via le menu des modules à l'accueil de Foundry : 
- installer le module Translation: French [Core] 
- installer le module libWrapper
- installer le module babele
- installer le module Translation: French [D&D5] 

via le menu configuration à l'accueil de Foundry :
- Langue par défaut : choisir Français - FR - Traduction du core

une fois votre monde lancé, dans paramètres => gestionnaire de modules 
- cocher Babele, libWrapper et fr-FR - DND5 System 
   et Sauvegarder :)

Votre jeu ( et celui de vos joueurs) sera normalement complètement en français

### Note : le fichier de langue 'zz' permet d'utiliser le module Translation Editor et pouvoir rapidement comparer fr / en